﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_24
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task 24 - Ashton Lye
            //Create a program that shows a menu where there user makes a selection
            //a. User makes a selection
            //b. Screen clears
            //c. User presses m or M and is taken back to the main menu

            var menu = 4;
            var userInput = "";

            do
            {
                Console.Clear();

                Console.WriteLine("+----------------------------+");
                Console.WriteLine("          Main Menu           ");
                Console.WriteLine("");
                Console.WriteLine("         1. Option 1          ");
                Console.WriteLine("         2. Option 2          ");
                Console.WriteLine("         3. Option 3          ");
                Console.WriteLine("");
                Console.WriteLine("+----------------------------+");
                Console.WriteLine("");
                Console.WriteLine("Please enter your selection:");
                menu = int.Parse(Console.ReadLine());

                if (menu == 1)
                {
                    Console.Clear();

                    Console.WriteLine("This is the result of option 1!");
                    Console.WriteLine("Enter m or M to return to the main menu");
                    Console.WriteLine("");
                    Console.WriteLine("Please enter your selection:");
                    userInput = Console.ReadLine();
                    if (userInput == "m" || userInput == "M")
                    {
                        menu = 4;
                    }
                }
                else if (menu == 2)
                {
                    Console.Clear();

                    Console.WriteLine("This is the result of option 2!");
                    Console.WriteLine("Enter m or M to return to the main menu");
                    Console.WriteLine("");
                    Console.WriteLine("Please enter your selection:");
                    userInput = Console.ReadLine();
                    if (userInput == "m" || userInput == "M")
                    {
                        menu = 4;
                    }
                }
                else if (menu == 3)
                {
                    Console.Clear();

                    Console.WriteLine("This is the result of option 3!");
                    Console.WriteLine("Enter m or M to return to the main menu");
                    Console.WriteLine("");
                    Console.WriteLine("Please enter your selection:");
                    userInput = Console.ReadLine();
                    if (userInput == "m" || userInput == "M")
                    {
                        menu = 4;
                    }
                }

                if (menu > 4)
                {
                    Console.WriteLine("Please enter a valid input");
                    Console.WriteLine("Press the Enter key to return to the menu");
                    Console.ReadLine();
                    menu = 4;
                }
            } while (menu == 4);

            
        }
    }
}
