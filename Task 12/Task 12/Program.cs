﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_12
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task 12 - Ashton Lye
            //Check if a number is a even or odd number  (yes or no answer to the user)
            var userNum = 0;

            Console.WriteLine("Please enter a number");
            userNum = int.Parse(Console.ReadLine());

            if ((userNum % 2) == 0)
            {
                Console.WriteLine("Your number is even!");
            }
            else
            {
                Console.WriteLine("Your number is odd!");
            }
        }
    }
}
