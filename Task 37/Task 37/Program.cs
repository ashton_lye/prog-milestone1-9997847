﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_37
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task 37 - Ashton Lye  
            //Write a program that calculates how many hours you should study per week for a 15 credit paper.
            //Use the following data to create your app.
            //a. There are 10 hours of study for every credit of the paper
            //b. Each week gives 5 contact hours(hours in lecture and tutorials)
            //c. The Semester is 12 weeks long.

            var courseCredits = 15;
            var classTime = 5;
            var weeksInSemester = 12;

            var totalHours = courseCredits * 10;
            var inClassHours = classTime * weeksInSemester;
            var homeHours = totalHours - inClassHours;
            var hoursPerWeek = homeHours / weeksInSemester;

            Console.WriteLine("Welcome to the study per week calculator!");
            Console.WriteLine($"You should study for 10 hours per credit a course is worth, so for a {courseCredits} credit course, that's {totalHours} hours!");
            Console.WriteLine($"You get {classTime} hours a week in class, so over {weeksInSemester} weeks that adds up to {inClassHours} hours.");
            Console.WriteLine($"That means you only need to do {homeHours} hours of study at home!");
            Console.WriteLine($"If you divide that up over the {weeksInSemester} weeks of the semester, you'll only have to do {hoursPerWeek} hours of study per week!");
            Console.WriteLine("");
        }
    }
}
