﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_22
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task 22 - Ashton Lye
            //Create a dictionary filled with fruits & vegetables listed.
            //The key should be the name of the fruit / vegetable & the value should be whether they are a fruit or vegetable.
            //a. Display to the user how many fruits there are and how many vegetables there are.
            //b. Also display which of the list are fruits

            var fruitDict = new Dictionary<string, string>();
            fruitDict.Add("Banana", "Fruit");
            fruitDict.Add("Apple", "Fruit");
            fruitDict.Add("Orange", "Fruit");
            fruitDict.Add("Carrot", "Vegetable");
            fruitDict.Add("Potato", "Vegetable");
            fruitDict.Add("Cabbage", "Vegetable");
            fruitDict.Add("Broccoli", "Vegetable");

            var fruitList = new List<string> { };
            var fruitCount = 0;
            var vegeCount = 0;

            foreach (var x in fruitDict)
            {
                if (x.Value == "Fruit")
                {
                    fruitList.Add(x.Key);
                    fruitCount++;
                }
                else
                {
                    vegeCount++;
                }
            }

            Console.WriteLine($"There are {fruitCount} Fruits and {vegeCount} Vegetables in the Dictionary");
            Console.WriteLine($"{string.Join(", ", fruitList)}");

        }
    }
}
