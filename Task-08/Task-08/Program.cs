﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_08
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task 08 - Ashton Lye
            //Forloop - print a countdown from 99 to 0 using no more than 5 lines of code in the loop block
            var counter = 0;
            var i = 100;

            for (i = 100; i > counter; i--)
            {
                var printNum = i - 1;
                Console.WriteLine($"This is line number {printNum}");
            }



        }
    }
}
