﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_26
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task 26 - Ashton Lye
            //Create an array with the following values and print it in descending order to the screen.
            //a. Red, Blue, Yellow, Green, Pink

            var colours = new string[5] { "Red", "Blue", "Yellow", "Green", "Pink" };
            Array.Reverse(colours);

            Console.WriteLine(string.Join(", ", colours));

        }
    }
}
