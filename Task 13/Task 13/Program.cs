﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_13
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task 13 - Ashton Lye
            //Mini shop - the sum of  3 doubles by user input and add GST (15%) 
            //Output must show $ signs
            var item1 = 0.0;
            var item2 = 0.0;
            var item3 = 0.0;
            var total = 0.0;
            var gstTotal = 0.0;

            Console.WriteLine("Welcome to the mini shop!");
            Console.WriteLine("Please enter the price of your first item, with decimals (e.g. 5.00 or 3.50):");
            item1 = double.Parse(Console.ReadLine());

            Console.WriteLine("Please enter the price of your second item, with decimals (e.g. 5.00 or 3.50):");
            item2 = double.Parse(Console.ReadLine());

            Console.WriteLine("Please enter the price of your third item, with decimals (e.g. 5.00 or 3.50):");
            item3 = double.Parse(Console.ReadLine());

            total = item1 + item2 + item3;
            gstTotal = total * 1.15;

            Console.WriteLine($"The total cost of your items today, including GST is ${gstTotal}");

        }
    }
}
