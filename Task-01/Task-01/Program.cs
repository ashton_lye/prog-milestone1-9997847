﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_01
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task 1 - Ashton Lye
            //Get user input for name and age and show 3 different ways to print a string
            var userName = "";
            var userAge = 0;

            Console.WriteLine("Hi! Please enter your name:");
            userName = Console.ReadLine();

            Console.WriteLine("Now enter your age:");
            userAge = int.Parse(Console.ReadLine());

            //Print method 1
            Console.WriteLine($"Thanks! Your name is {userName} and your age is {userAge}.");

            //Print method 2
            Console.WriteLine("Thanks! Your name is " + userName + " and your age is " + userAge + ".");

            //Print method 3
            Console.WriteLine("Thanks! Your name is {0} and your age is {1}.", userName, userAge);
        }
    }
}
