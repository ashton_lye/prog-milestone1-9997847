﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_19
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task 19 - Ashton Lye  
            //You are presented with an array of numbers
            //The array for you to use has the following values(34, 45, 21, 44, 67, 88, 86)
            //Put in all the even numbers in a new list
            //Print the List

            var numbers = new int[7] { 34, 45, 21, 44, 67, 88, 86 };
            var evenNumbers = new List<int> { };
            var i = 0;
            var counter = 7;

            for (i = 0; i < counter; i++)
            {
                if (numbers[i] % 2 == 0)
                {
                    evenNumbers.Add(numbers[i]);
                }
            }
            Console.WriteLine(string.Join(", ", evenNumbers));
        }
    }
}
