﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_29
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task 29 - Ashton Lye
            //Count the number of words in a string
            //a. The quick brown fox jumped over the fence

            var sentence = "The quick brown fox jumped over the fence";
            var splitSentence = sentence.Split(' ');
            Console.WriteLine($"The sentence is {splitSentence.Length} words long");

        }
    }
}
