﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_35
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task 35 - Ashton Lye
            //Write a program that asks for 5 different maths questions based on 3 numbers by user input. Display the answer on the screen.

            var userNum1 = 0;
            var userNum2 = 0;
            var userNum3 = 0;

            Console.WriteLine("Please enter a number to be used in some equations:");
            userNum1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Please enter a second number to be used in the equations:");
            userNum2 = int.Parse(Console.ReadLine());

            Console.WriteLine("Please enter a third number to be used in the equations:");
            userNum3 = int.Parse(Console.ReadLine());

            Console.WriteLine($"{userNum1} + {userNum2} + {userNum3} = {userNum1 + userNum2 + userNum3}");
            Console.WriteLine($"{userNum1} - {userNum2} - {userNum3} = {userNum1 - userNum2 - userNum3}");
            Console.WriteLine($"{userNum1} * {userNum2} * {userNum3} = {userNum1 * userNum2 * userNum3}");
            Console.WriteLine($"{userNum1} / {userNum2} / {userNum3} = {userNum1 / userNum2 / userNum3}");
            Console.WriteLine($"{userNum1} + {userNum2} * {userNum3} = {userNum1 + userNum2 * userNum3}");
        }
    }
}
