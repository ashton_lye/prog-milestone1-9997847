﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_31
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task 31 - Ashton Lye
            //Write a program that checks if a number is divisible by 3 and 4 without leaving any decimal places. Inform the user of the number

            var userNum = 0;

            Console.WriteLine("Please enter a number to find out if it is divisible by both 3 and 4:");
            userNum = int.Parse(Console.ReadLine());

            if (userNum % 3 == 0 && userNum % 4 == 0)
            {
                Console.WriteLine($"Yes! {userNum} is divisible by both 3 and 4.");
            }
            else
            {
                Console.WriteLine($"Oh no! {userNum} is not divisible by both 3 and 4.");
            }
        }
    }
}
