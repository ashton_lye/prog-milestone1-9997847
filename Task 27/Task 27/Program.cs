﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_27
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task 27 - Ashton Lye
            //Create an array with the following values and print it in ascending order to the screen.
            //a. Red, Blue, Yellow, Green, Pink

            var colours = new string[5] { "Red", "Blue", "Yellow", "Green", "Pink" };
            Array.Sort(colours);

            Console.WriteLine(string.Join(", ", colours));
        }
    }
}
