﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_20
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task 20 - Ashton Lye  
            //You are presented with an array of numbers
            //The array for you to use has the following values(33, 45, 21, 44, 67, 87, 86)
            //a. Put in all the odd numbers in a new list
            //b. Print the List

            var numbers = new int[7] { 33, 45, 21, 44, 67, 87, 86 };
            var oddNumbers = new List<int> { };
            var i = 0;
            var counter = 7;

            for (i = 0; i < counter; i++)
            {
                if (numbers[i] % 2 != 0)
                {
                    oddNumbers.Add(numbers[i]);
                }
            }
            Console.WriteLine(string.Join(", ", oddNumbers));
        }
    }
}
