﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_09
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task 09 - Ashton Lye
            //Write a program that shows which years are a leaps year for the next 20 years
            var currentYear = 0;
            var i = 0;
            var counter = 20;

            Console.WriteLine("Please enter the current year:");
            currentYear = int.Parse(Console.ReadLine());
            
            while (i < counter)
            {
                if ((currentYear % 4) == 0)
                {
                    Console.WriteLine($"{currentYear} is a leap year!");
                }
                else
                {
                    Console.WriteLine($"{currentYear} is not a leap year!");
                }
                currentYear++;
                i++;
            }
        }
    }
}
