﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_18
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task 18 - Ashton Lye
            //Create a List<T> that holds people’s name and month and birthday.
            //a. Add 3 persons to it
            //b. Print them out to the screen

            var nameList = new List<Tuple<string, string, int>>();

            nameList.Add(Tuple.Create("Ashton", "September", 18));
            nameList.Add(Tuple.Create("Kelly", "August", 19));
            nameList.Add(Tuple.Create("Laurence", "April", 18));

            Console.WriteLine($"{nameList[0]}, {nameList[1]}, {nameList[2]}");
        }
    }
}
