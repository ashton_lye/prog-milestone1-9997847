﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_10
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task 10 - Ashton Lye
            //Write a program that tells you how many leap years are there in the next 20 years (with calculation)
            var currentYear = 0;
            var leapYears = 0;
            var i = 0;
            var counter = 20;

            Console.WriteLine("Please enter the current year:");
            currentYear = int.Parse(Console.ReadLine());

            while (i < counter)
            {
                if ((currentYear % 4) == 0)
                {
                    leapYears++;
                }
                currentYear++;
                i++;
            }
            Console.WriteLine($"There are {leapYears} leap years in the next 20 years.");
        }
    }
}
