﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_30
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task 30 - Ashton Lye
            //Take in 2 number from a user and show the difference when they are added as a string and as a number

            var strNum1 = " ";
            var strNum2 = " ";
            var intNum1 = 0;
            var intNum2 = 0;

            Console.WriteLine("Please enter a number:");
            strNum1 = Console.ReadLine();
            intNum1 = int.Parse(strNum1);

            Console.WriteLine("Please enter a second number:");
            strNum2 = Console.ReadLine();
            intNum2 = int.Parse(strNum2);

            Console.WriteLine($"The sum of your two numbers as a string is {strNum1 + strNum2}");
            Console.WriteLine($"The sum of your two numbers as an integer is {intNum1 + intNum2}");


        }
    }
}
