﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_36
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task 36 - Ashton Lye
            //Write a program that has a simple for loop with a counter, but in the block has an if statement that gives feedback on whether the counter is equal to the index. Give different feedback when the loop is complete.

            var counter = 10;
            var i = 0;

            for (i = 0; i < counter; i++)
            {
                if (i != counter)
                {
                    Console.WriteLine($"The index is not equal to the counter {i}/{counter}");
                }
            }
            Console.WriteLine("The counter and index are equal, the loop is complete.");
        }
    }
}
